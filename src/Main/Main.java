package Main;

import This.Triangulo;

public class Main {

    public static void main(String[] args) {
        Triangulo triangulo1 = new Triangulo();
        Triangulo triangulo2 = new Triangulo(30,40);
        Triangulo triangulo3 = new Triangulo(4, 5, 50, 80);

        System.out.println(triangulo1.toString());
        System.out.println(triangulo2.toString());
        System.out.println(triangulo3.toString());

        Triangulo.calcularPerimetro(40);

        System.out.println(triangulo1.toString());
        System.out.println(triangulo2.toString());
        System.out.println(triangulo3.toString());

        triangulo3.calcularArea();
        triangulo3.calcularArea(21);
        triangulo3.calcularArea(32, 49);


    }
}
