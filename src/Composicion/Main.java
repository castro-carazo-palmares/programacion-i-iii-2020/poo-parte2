package Composicion;

public class Main {
    public static void main(String[] args) {
        Direccion direccion = new Direccion("Alajuela", "Naranjo", "San Miguel", "150 metros sur de la Escuela.");

        Persona persona = new Persona("Sigifredo", "Valles Castro", 45, 181, direccion);

        System.out.println(persona.toString());

        cambiarNombre(persona);

        System.out.println(persona.toString());


        System.out.println(persona.getDireccion().getCanton());
        int colones = 4000;

        System.out.println(colones);
        restarColones(colones);

        System.out.println(colones);


    }

    public static void restarColones(int colones) {
        colones = colones - 1000;
    }

    public static void cambiarNombre(Persona persona) {
        persona.setNombre("Ronulfo");
    }
}
