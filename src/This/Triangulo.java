package This;

public class Triangulo {

    private int x, y, base, altura;
    public static double perimetro;

    public Triangulo() {
        this(0, 0, 0, 0);
    }

    public Triangulo(int base, int altura) {
        this(0, 0, base, altura);
    }

    public Triangulo(int x, int y, int base, int altura) {
        this.x = x;
        this.y = y;
        this.base = base;
        this.altura = altura;
    }

    public void calcularArea() {
        double area = ((double) this.base * this.altura) / 2;
        System.out.println(area);
    }

    public void calcularArea(int base) {
        double area = ((double) base * this.altura) / 2;
        System.out.println(area);
    }

    public void calcularArea(int base, int altura) {
        double area = ((double) base * altura) / 2;
        System.out.println(area);
    }

    public static void calcularPerimetro(int base) {
        perimetro = base * 3;
    }

    @Override
    public String toString() {
        return "Triangulo{" +
                "x=" + x +
                ", y=" + y +
                ", base=" + base +
                ", altura=" + altura +
                ", perimetro=" + perimetro +
                '}';
    }
}
